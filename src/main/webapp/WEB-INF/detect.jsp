<html>
<head>
<title>getMobileOSver USER_AGENT OS version test v1.10 - 2014-Oct-17</title>
<style>
body { background-color:#202020; font-family:monospace; color:lightgreen; }
</style>
<!-- <script type="text/javascript" src="getMobileOSver.min.js"></script> -->
</head>
<body>
<script>
var getMobileOS = {
		init : function(e) {
            var t, n;
            if (e) {
                t = e
            } else {
                t = navigator.userAgent
            }
            if (t.match(/iPad/i)) {
                getMobileOS.device = "iPad";
                getMobileOS.os = "ios";
                n = t.indexOf("OS ")
            } else if (t.match(/iPod/i)) {
                getMobileOS.device = "iPod";
                getMobileOS.os = "ios";
                n = t.indexOf("OS ")
            } else if (t.match(/iPhone/i)) {
                getMobileOS.device = "iPhone";
                getMobileOS.os = "ios";
                n = t.indexOf("OS ")
            } else if (t.match(/Android/i)) {
                getMobileOS.device = "Android";
                getMobileOS.os = "android";
                n = t.indexOf("Android ")
            } else {
                getMobileOS.device = "unknown";
                getMobileOS.os = false
            }
            if (getMobileOS.os === "ios" && n > -1) {
                getMobileOS.ver = t.substr(n + 3, 5).replace("_", ".");
                return true
            } else if (getMobileOS.os === "android") {
                getMobileOS.ver = t.substr(n + 8, 5);
                return true
            } else {
                getMobileOS.ver = false;
                return false
            }
        }
    }

function testUAstring( opt ) {
  if ( opt ) {
    getMobileOS.init(opt);
  }
  else {
    getMobileOS.init();
    opt = navigator.userAgent;
  }
  document.write( "<p>" + opt + "<br>" );
  document.write( "OS: " + getMobileOS.os + " ver: " + getMobileOS.ver + " device: " + getMobileOS.device );

  if ( getMobileOS.os === "ios" ) {
    if ( Number( getMobileOS.ver.charAt(0) ) >= 6 ) {
      document.write( " (iOS is version 6 or greater)");
    }
  }
  else if ( getMobileOS.os === "android" ) {
    if ( Number( getMobileOS.ver ) >= 4.4 ) {
      document.write( " (Android is version 4.4 (KitKat) or greater)");
    }
  }
  else {
    document.write( " !!! unknown mobile OS !!!");
  }
  document.write( "<br>" );
}

document.write( "YOUR USER AGENT STRING:<br>");
testUAstring();
/* document.write( "<br>TEST USER AGENT STRINGS:<br>");
testUAstring( "Mozilla/5.0 (iPad; U; CPU OS 4_3_3 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Mobile/8J2" );
testUAstring( "Mozilla/5.0 (iPad; U; CPU OS 4_3_5 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Mobile/8L1" );
testUAstring( "Mozilla/5.0 (iPad; CPU OS 5_0_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Mobile/9A405" );
testUAstring( "Mozilla/5.0 (iPad; CPU OS 5_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Mobile/9B176" );
testUAstring( "Mozilla/5.0 (iPad; CPU OS 5_1_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Mobile/9B206" );
testUAstring( "Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10A403" );
testUAstring( "Mozilla/5.0 (iPad; CPU OS 6_0_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10A523" );
testUAstring( "Mozilla/5.0 (iPad; CPU OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Mobile/11D257 (375606336)" );
testUAstring( "Mozilla/5.0 (iPad; CPU OS 8_0_2 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Mobile/12A405 (374751968)" );
testUAstring( "Mozilla/5.0 (iPhone; CPU iPhone OS 5_0_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Mobile/9A405" );
testUAstring( "Mozilla/5.0 (iPhone; CPU iPhone OS 5_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Mobile/9B176" );
testUAstring( "Mozilla/5.0 (iPhone; CPU iPhone OS 5_1_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Mobile/9B206" );
testUAstring( "Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10A403" );
testUAstring( "Mozilla/5.0 (iPhone; CPU iPhone OS 6_0_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10A523" );
testUAstring( "Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Mobile/11D257 (383425840)" );
testUAstring( "Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Mobile/12A365 (401099904)" );
testUAstring( "Mozilla/5.0 (iPod; CPU iPhone OS 5_1_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Mobile/9B206" );
testUAstring( "Mozilla/5.0 (iPod; CPU iPhone OS 6_0_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10A523" );
testUAstring( "Mozilla/5.0 (Linux; U; Android 2.3.6; en-us; T-Mobile myTouch Build/HuaweiU8680) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1" );
testUAstring( "Mozilla/5.0 (Linux; U; Android 2.3.6; ru-ru; GT-I8160 Build/GINGERBREAD) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1" );
testUAstring( "Mozilla/5.0 (Linux; U; Android 4.0.3; en-us; HTC Glacier Build/IML74K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30" );
testUAstring( "Mozilla/5.0 (Linux; U; Android 4.0.3; en-us; HTC PH39100 Build/IML74K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30" );
testUAstring( "Mozilla/5.0 (Linux; U; Android 4.0.3; en-us; SAMSUNG-SGH-I777 Build/IML74K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30" );
testUAstring( "Mozilla/5.0 (Linux; U; Android 4.0.3; es-us; HTC_Amaze_4G Build/IML74K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30" );
testUAstring( "Mozilla/5.0 (Linux; U; Android 4.0.4; en-us; C5155 Build/IML77) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30" );
testUAstring( "Mozilla/5.0 (Linux; U; Android 4.0.4; en-us; SAMSUNG-SGH-I747 Build/IMM76D) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30" );
testUAstring( "Mozilla/5.0 (Linux; U; Android 4.0.4; en-us; SCH-I200 Build/IMM76D) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30" );
testUAstring( "Mozilla/5.0 (Linux; U; Android 4.0.4; en-us; SGH-T999 Build/IMM76D) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30" );
testUAstring( "Mozilla/5.0 (Linux; U; Android 4.0.4; en-us; SPH-D710 Build/IMM76I) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30" );
testUAstring( "Mozilla/5.0 (Linux; U; Android 4.0.4; es-es; HTC Sensation Build/IMM76L; CyanogenMod-9.1.0) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30" );
testUAstring( "Mozilla/5.0 (Linux; U; Android 4.1.1; en-us; Galaxy Nexus Build/JRO03U) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30" );
testUAstring( "Mozilla/5.0 (Linux; U; Android 4.1.1; en-us; SPH-L710 Build/JRO03L) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30" );
testUAstring( "Mozilla/5.0 (Linux; U; Android 4.2; en-us; Nexus 7 Build/JOP40C) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30" );
testUAstring( "Mozilla/5.0 (Linux; U; Android 4.3; en-us; SGH-T889 Build/JSS15J) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30" );
testUAstring( "Mozilla/5.0 (Linux; Android 4.4.4; Nexus 4 Build/KTU84P) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/33.0.0.0 Mobile Safari/537.36" );
testUAstring( "Mozilla/5.0 (Linux; Android 4.4.3; HTC One Build/KTU84L) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/33.0.0.0 Mobile Safari/537.36" );
// browser test string
testUAstring( "Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.1 (KHTML, like Gecko) Ubuntu/10.04 Chromium/14.0.808.0 Chrome/14.0.808.0 Safari/535.1" ); */

</script>
</body>
</html>