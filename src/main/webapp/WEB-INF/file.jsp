<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
 
<body> 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<h2>Spring MVC - Uploading a file.. </h2>
	<form:form method="POST" commandName="file"	enctype="multipart/form-data">
 
		Upload your file please: 
		<input type="file" name="file" />
		<input type="submit" value="upload" />
		<form:errors path="file" cssStyle="color: #ff0000;" />
	</form:form>
 
    <div class="progress">
        <div class="bar"></div >
        <div class="percent">0%</div >
    </div>

<div id="status"></div>
 <SCRIPT LANGUAGE = "Javascript">
    $(function() {

    var bar = $('.bar');
    var percent = $('.percent');
    var status = $('#status');

    $('form').ajaxForm({
        beforeSend: function() {
            status.empty();
            var percentVal = '0%';
            bar.width(percentVal);
            percent.html(percentVal);
        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal);
            percent.html(percentVal);
        },
        complete: function(xhr) {
            status.html(xhr.responseText);
        }
    });
}); 
</SCRIPT>
</body>
</html>