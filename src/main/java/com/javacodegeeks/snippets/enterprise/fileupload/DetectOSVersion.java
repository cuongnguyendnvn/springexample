package com.javacodegeeks.snippets.enterprise.fileupload;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/detect.htm")
public class DetectOSVersion {

	@RequestMapping(method = RequestMethod.GET)
	public String getForm() {
		return "detect";
	}
}
