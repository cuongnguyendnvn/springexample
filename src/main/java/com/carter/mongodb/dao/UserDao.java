package com.carter.mongodb.dao;

import com.carter.mongodb.user.Users;

public interface UserDao {
	public void create(Users u);
    
    public Users readById(String id);
     
    public void update(Users u);
     
    public void deleteById(String id);
}
