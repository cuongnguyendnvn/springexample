package com.carter.mongodb.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.carter.mongodb.user.Users;

@Component
public class UserDaoImpl implements UserDao {

	@Autowired
    private MongoOperations mongoOps;
    private static final String USER_COLLECTION = "Users";

    public UserDaoImpl() {}

    public UserDaoImpl(MongoOperations mongoOps) {
        this.mongoOps = mongoOps;
    }

    @Override
    public void create(Users u) {
        this.mongoOps.insert(u, USER_COLLECTION);
    }

    @Override
    public Users readById(String id) {
        Query query = new Query(Criteria.where("_id").is(id));
        return this.mongoOps.findOne(query, Users.class, USER_COLLECTION);
    }

    @Override
    public void update(Users u) {
        this.mongoOps.save(u, USER_COLLECTION);
    }

    @Override
    public void deleteById(String id) {
        Query query = new Query(Criteria.where("_id").is(id));
        this.mongoOps.remove(query, USER_COLLECTION);
    }

}
