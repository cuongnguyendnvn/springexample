package com.carter.controller;

import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.carter.ultils.JsonUtil;

@Controller
@RequestMapping("mock_resource")
public class MockResouceServer {

    @RequestMapping(value = "/mock_api", method = RequestMethod.POST)
    public @ResponseBody String resourceRequest(
            @RequestParam(value = "mock_param") String mock_param,
            @RequestParam(value = "access_token") String access_token) {
        RestTemplate restTemplate = new RestTemplate();

        String url = "http://54.179.169.46/oauth/satay/validatetoken";
        MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
        params.add("client_id", "ZipitChat");
        params.add("masking_id", "abc");
        params.add("access_token", access_token);

        String validateResult = restTemplate.postForObject(url, params, String.class);

        return checkValidateAccessTokenResponseString(validateResult);
    }

    protected String checkValidateAccessTokenResponseString(String validateResult) {
        ValidateAccessTokenResult result = null;
        try {
            result = JsonUtil.toObject(validateResult, ValidateAccessTokenResult.class);
        } catch (IOException e1) {
            return "Technical issue";
        }

        if ("E0000".equalsIgnoreCase(result.getStatusCode())) {
            return "Yeah!!! Here is your resource";
        } else {
            return result.getStatusDescription();
        }
    }

    public static class ValidateAccessTokenResult {
        private String statusCode;
        private String statusDescription;

        public ValidateAccessTokenResult() {}

        public ValidateAccessTokenResult(String statusCode, String statusDescription) {
            this.statusCode = statusCode;
            this.statusDescription = statusDescription;
        }

        public String getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        public String getStatusDescription() {
            return statusDescription;
        }

        public void setStatusDescription(String statusDescription) {
            this.statusDescription = statusDescription;
        }
    }
}
