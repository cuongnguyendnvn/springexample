package com.carter.controller;

import java.net.URI;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

@Controller
@RequestMapping("/mock_client")
public class MockClient {

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody String restTemplateTest() {
        HttpHost host = new HttpHost("localhost", 8080, "http");
        RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactoryBasicAuth(host));

        HttpComponentsClientHttpRequestFactory requestFactory = (HttpComponentsClientHttpRequestFactory) restTemplate.getRequestFactory();

        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(
                new AuthScope(host, AuthScope.ANY_REALM, AuthScope.ANY_SCHEME),
                new UsernamePasswordCredentials("ZipitChat", "secret"));

        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);

        HttpClient httpClient = httpClientBuilder.build();
        requestFactory.setHttpClient(httpClient);

        String url = "http://localhost:8080/identityserver/oauth/token";
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("grant_type", "password");
        map.add("masking_id", "G014D9D2");
        map.add("pwd", "bcb15f821479b4d5772bd0ca866c00ad5f926e3580720659cc80d39c9d09802a");

        try {
            String result = restTemplate.postForObject(url, map, String.class);
            System.out.println("RESULT: " + result);

            return result;
        } catch(Exception e) {
            System.out.println("ERROR: " + e.toString());
            return e.getMessage();
        }
    }

    public class HttpComponentsClientHttpRequestFactoryBasicAuth 
      extends HttpComponentsClientHttpRequestFactory {
     
        HttpHost host;
     
        public HttpComponentsClientHttpRequestFactoryBasicAuth(HttpHost host) {
            super();
            this.host = host;
        }

        protected HttpContext createHttpContext(HttpMethod httpMethod, URI uri) {
            return createHttpContext();
        }

        private HttpContext createHttpContext() {
            // Create AuthCache instance
            AuthCache authCache = new BasicAuthCache();
            // Generate BASIC scheme object and add it to the local auth cache
            BasicScheme basicAuth = new BasicScheme();
            authCache.put(host, basicAuth);
     
            // Add AuthCache to the execution context
            BasicHttpContext localcontext = new BasicHttpContext();
            localcontext.setAttribute(HttpClientContext.AUTH_CACHE, authCache);
            return localcontext;
        }
    }
}
