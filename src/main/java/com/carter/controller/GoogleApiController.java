package com.carter.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.oauth2.Oauth2;
import com.google.api.services.oauth2.model.Userinfoplus;

@Controller
public class GoogleApiController {

	@Value("${auth.accesstoken}")
	private String accessToken;

	@RequestMapping(value = "/google/authcode", method = RequestMethod.GET)
	public ModelAndView showAuthorizationCode(
			@RequestParam(value = "code", required = true) String code){
		ModelAndView model = new ModelAndView("/google/authcode");
		model.addObject("authCode", code);
		return model;
	}

	@RequestMapping(value = "/google/profile", method = RequestMethod.GET)
	public ModelAndView showGooglePlusProfile() throws IOException {
		ModelAndView model = new ModelAndView("/google/plusprofile");

		GoogleCredential credential = new GoogleCredential().setAccessToken(accessToken);
		Oauth2 oauth2 = new Oauth2.Builder(new NetHttpTransport(), new JacksonFactory(), credential).setApplicationName("Oauth2").build();
		Userinfoplus userinfo = oauth2.userinfo().get().execute();

		model.addObject("userInfo", userinfo.toPrettyString());

		return model;
	}
}
