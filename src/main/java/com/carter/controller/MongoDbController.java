package com.carter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.carter.mongodb.dao.UserDao;
import com.carter.mongodb.user.Users;

@Controller
@RequestMapping("/mongodb/create.htm")
public class MongoDbController {
	@Autowired
	UserDao userDao;
	
	@RequestMapping(method = RequestMethod.POST)
	public String create() {
		Users u = new Users("Cuong", "NGUYEN");
		userDao.create(u);

		return "mongodb_create_success";
	}
}
