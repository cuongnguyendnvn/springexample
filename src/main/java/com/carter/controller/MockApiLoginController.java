package com.carter.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/mock_login")
public class MockApiLoginController {

    public static final String OK = "2690";
    public static final String USER_NOT_FOUND = "2691";
    public static final String USER_NOT_FOUND_DESC = "User not found";
    public static final String PASSWORD_IS_WRONG = "2692";
    public static final String PASSWORD_IS_WRONG_DESC = "Password is wrong";
    public static final String ANY_EXCEPTION = "2693";
    public static final String ANY_EXCEPTION_DESC = "Technical issue";

    private String masking_id = "abc";
    private String pwd = "def";

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody LoginResponse loginApi(
            @RequestParam(value = "masking_id", required = true) String masking_id,
            @RequestParam(value = "pwd", required = true) String pwd) {
        LoginResponse response = new LoginResponse();

        if(this.masking_id.equals(masking_id) && this.pwd.equals(pwd)) {
            response.setStatus(OK);
        } else {
            if (!this.masking_id.equals(masking_id)) {
                response.setStatus(USER_NOT_FOUND);
                response.setDescription(USER_NOT_FOUND_DESC);
            } else if (!this.pwd.equals(pwd)) {
                response.setStatus(PASSWORD_IS_WRONG);
                response.setDescription(PASSWORD_IS_WRONG_DESC);
            } else {
                response.setStatus(ANY_EXCEPTION);
                response.setDescription(ANY_EXCEPTION_DESC);
            }
            
        }

        return response;
    }

    public static class LoginResponse {
        private String status;
        private String description;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}
