/*
 * Copyright © 2015 mTouche™. All rights reserved.
 */

package com.carter.ultils;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The util class to work with json
 * @author Vincent : 20150721
 * @version 1.0.0
 */
public class JsonUtil {
    public static String toJson(Object obj) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(obj);
    }
    
    public static <T> T toObject(String json, Class<T> clazz) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, clazz);
    }
}
